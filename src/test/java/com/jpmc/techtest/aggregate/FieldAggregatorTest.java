package com.jpmc.techtest.aggregate;

import com.jpmc.techtest.model.AggregationType;
import com.jpmc.techtest.model.MerchantInput;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class FieldAggregatorTest {

    @Test
    void shouldAggregateBasedOnInput() {
        //arrange-act
        var aggregates = FieldAggregator.aggregate(merchantInputs(), AggregationType.ALL);

        //assert
        assertThat(aggregates).asList().hasSize(2);
        assertThat(aggregates).asList().contains("2022-02-23,MERCHANT,R,500,4");
        assertThat(aggregates).asList().contains("2022-02-23,INCOME,R,-200,4");
    }

    private static List<MerchantInput> merchantInputs() {
        return Arrays.asList(
                MerchantInput.builder()
                        .creditDebitCode("C")
                        .accountCode("AAA")
                        .accountType("R")
                        .actionDate(LocalDate.of(2022, 2, 23))
                        .accountCode("MERCHANT")
                        .amount(new BigDecimal(1500))
                        .decimal(4)
                        .build(),
                MerchantInput.builder()
                        .creditDebitCode("D")
                        .accountCode("BBB")
                        .accountType("R")
                        .actionDate(LocalDate.of(2022, 2, 23))
                        .accountCode("MERCHANT")
                        .amount(new BigDecimal(1000))
                        .decimal(4)
                        .build(),
                MerchantInput.builder()
                        .creditDebitCode("D")
                        .accountCode("CCC")
                        .accountType("R")
                        .actionDate(LocalDate.of(2022, 2, 23))
                        .accountCode("INCOME")
                        .amount(new BigDecimal(200))
                        .decimal(4)
                        .build()
        );
    }
}
