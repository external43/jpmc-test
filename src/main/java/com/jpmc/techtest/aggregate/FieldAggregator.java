package com.jpmc.techtest.aggregate;

import com.jpmc.techtest.model.AggregationType;
import com.jpmc.techtest.model.MerchantInput;
import lombok.experimental.UtilityClass;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@UtilityClass
public class FieldAggregator {

    public static List<String> aggregate(List<MerchantInput> inputs, AggregationType aggregationType) {
        var aggregates = inputs.stream()
                .collect(Collectors.groupingBy(aggregationType.getGroupByKeyExtractor()))
                .values().stream()
                .map(aggList -> buildAggregate(aggList, aggregationType))
                .collect(Collectors.toList());

        System.out.println(aggregationType.getHeaders());
        aggregates.forEach(System.out::println);

        //returning value from this method to allow for unit test assertions
        return aggregates;
    }

    private static String buildAggregate(List<MerchantInput> lines, AggregationType aggregationType) {
        var amount = lines.stream()
                .map(FieldAggregator::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        return String.join(",",
                lines.stream().limit(1).map(aggregationType.getValueCombiner()).collect(Collectors.joining(",")),
                String.valueOf(amount),
                String.valueOf(lines.get(0).getDecimal())
        );
    }

    private static BigDecimal getAmount(MerchantInput line) {
        if (line.getCreditDebitCode().equalsIgnoreCase("C")) {
            return line.getAmount();
        } else {
            return line.getAmount().negate();
        }
    }
}
