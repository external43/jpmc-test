package com.jpmc.techtest.parser;

import com.jpmc.techtest.model.MerchantInput;
import org.apache.commons.csv.CSVFormat;

import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class CsvReader {

    public List<MerchantInput> read() throws IOException {
        String[] headers = { "Merchant","PaymentId","Date","TransactionId","TransactionTs","Stage","Method",
                "MethodCode", "AccountCode","ServiceId","ServiceCategory","AccountType","CreditDebitCode","Amount",
                "Decimal","Version","ActionDate","EntryDate","BatchId","Entity"};

        var in = new FileReader("src/main/resources/input.csv");
        var records = CSVFormat.DEFAULT
                .withHeader(headers)
                .withFirstRecordAsHeader()
                .parse(in);

        var stream = StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(
                        records.iterator(),
                        Spliterator.ORDERED), false);

        return stream.map(line ->
                MerchantInput.builder()
                        .accountCode(line.get(8))
                        .accountType(line.get(11))
                        .actionDate(LocalDate.parse(line.get(17)))
                        .creditDebitCode(line.get(12))
                        .amount(new BigDecimal(line.get(13)))
                        .decimal(Integer.parseInt(line.get(15)))
                        .build()
        ).collect(Collectors.toList());
    }
}
