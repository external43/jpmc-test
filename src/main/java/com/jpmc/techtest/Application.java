package com.jpmc.techtest;

import com.jpmc.techtest.aggregate.FieldAggregator;
import com.jpmc.techtest.model.AggregationType;
import com.jpmc.techtest.parser.CsvReader;

import java.io.IOException;

public class Application {
    public static void main(String[] args) {
        var csvReader = new CsvReader();
        try {
            var inputList = csvReader.read();
            FieldAggregator.aggregate(inputList, AggregationType.ALL);
            System.out.println();
            FieldAggregator.aggregate(inputList, AggregationType.ACC_CODE);
            System.out.println();
            FieldAggregator.aggregate(inputList, AggregationType.ACC_CODE_AND_ACTION_DATE);
        } catch (IOException ex) {
            System.out.println("Could not parse the file.");
        }
    }
}
