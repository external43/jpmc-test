package com.jpmc.techtest.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;

@RequiredArgsConstructor
@Getter
public enum AggregationType {
    ACC_CODE(
            input -> Collections.singletonList(input.getAccountCode()),
            MerchantInput::getAccountCode,
            "AccountCode, Amount, Decimal"
            ),
    ACC_CODE_AND_ACTION_DATE(
            input -> List.of(input.getAccountCode(), input.getActionDate()),
            input -> String.join(",", String.valueOf(input.getActionDate()), input.getAccountCode()),
            "ActionDate, AccountCode, Amount, Decimal"
    ),
    ALL(
            input -> List.of(input.getAccountCode(), input.getActionDate()),
            input -> String.join(",", String.valueOf(input.getActionDate()), input.getAccountCode(), input.getAccountType()),
            "ActionDate, AccountCode, AccountType, Amount, Decimal"
            );

    private final Function<MerchantInput, List<Object>> groupByKeyExtractor;

    private final Function<MerchantInput, String> valueCombiner;

    private final String headers;

}
