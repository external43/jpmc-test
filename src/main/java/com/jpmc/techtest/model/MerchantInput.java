package com.jpmc.techtest.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
public class MerchantInput {
    /*
    * Merchant,PaymentId,Date,TransactionId,TransactionTs,Stage,Method,MethodCode,
    AccountCode,ServiceId,ServiceCategory,AccountType,CreditDebitCode,Amount,
    ,Decimal,Version,ActionDate,EntryDate,BatchId,Entity
     */
    private String accountCode;
    private String accountType;
    private String creditDebitCode;
    private BigDecimal amount;
    private int decimal;
    private LocalDate actionDate;
}
