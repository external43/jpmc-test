package com.jpmc.techtest.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class Aggregate {
    private String key;
    private String value;
}
